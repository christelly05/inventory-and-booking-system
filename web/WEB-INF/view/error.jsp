<%-- 
    Document   : error
    Created on : 08 15, 18, 7:22:34 PM
    Author     : user
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>OH NO!</title>
    </head>
    <body>
        <h1>Error Page</h1>
        <c:if test='${error != null}'> 
          <h2>${error.toString()}</h2>  
        </c:if>
    </body>
</html>
