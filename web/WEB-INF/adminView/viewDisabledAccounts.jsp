<%-- 
    Document   : viewEnableAccounts
    Created on : 08 19, 18, 3:19:33 PM
    Author     : user
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Goldland Millenia Suites Booking and Inventory Tracking System</title>
    </head>
    <body>
        <h1>Disabled Accounts</h1>
        <a href="../admin/ViewAccounts">All Accounts List</a>
        <a href="../admin/ViewEnable">Enabled Accounts List</a>
        
        <c:choose>
            <c:when test='${data == null} || ${data.size() == 0}'>
                <em>No Accounts Available</em><br />
            </c:when>
            <c:otherwise>
                <table border="1" align="center">
                    <tr>
                        <th>Employee Number</th>
                        <th>Username</th>
                        <th>Account Type</th>
                        <th>Account Status</th>
                    </tr>
                <c:forEach items="${data}" var="row">
                    <tr>
                        <td>${row.getEmployee_number()}</td>
                        <td>${row.getUsername()}</td>
                        <td>${row.getAccount_type()}</td>
                        <td>${row.getIsEnabled()}</td>
                    </tr>
                </c:forEach>
                </table>
            </c:otherwise>
        </c:choose>
    </body>
</html>
