<%-- 
    Document   : addEmployee
    Created on : 08 19, 18, 6:46:01 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add Employee Information</title>
    </head>
    <body>
        <h1>Personal Information</h1>
        <form action="../AddEmployee" method="POST">
            Employee Name:<input type="text" name="e_name" placeholder="Enter Employee Name" autocomplete="off" required/><br/>
            Department Number:<input type="number" name="dept_number" placeholder="Enter Department Number" autocomplete="off" required/><br/>
            Contact Number:<input type="text" name="contact_number" placeholder="Enter Contact Number" autocomplete="off" required/><br/>
            Email:<input type="text" name="e_email_address" placeholder="Enter Email" autocomplete="off" required/><br/>
            <input type="submit" name="Submit"/>
        </form>
    </body>
</html>
