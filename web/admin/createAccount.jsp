<%-- 
    Document   : createAccount
    Created on : 08 15, 18, 12:35:16 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Goldland Millenia Booking and Inventory System</title>
    </head>
    <body>
        <h1>Input Account Information</h1>
        <form action="../admin/AddAccount" method="POST">
            Employee Name:<input type="text" name="name" placeholder="Enter Employee Name" autocomplete="off" required/><br/>
            Username:<input type="text" name="uname" placeholder="Enter Username" autocomplete="off" required/><br/>
            Password:<input type="text" name="pass" placeholder="Enter Password" autocomplete="off" required/><br/>
            Account Type:
            <select name="type">
                <option value = "Admin">Admin</option>
                <option value = "Front Desk">Front Desk</option>
                <option value = "Department Head">Department Head</option>
                <option value = "Senior Manager">Senior Manager</option>
                <option value = "Inventory Manager">Inventory Manager</option>
            </select>
            <input type="submit" name="Enter"/>
        </form>
    </body>
</html>
