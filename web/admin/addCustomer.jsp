<%-- 
    Document   : addCustomer
    Created on : 08 17, 18, 4:15:57 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add Customer Information</title>
    </head>
    <body>
        <h1>Personal Information</h1>
        <form action="../AddCustomer" method="POST">
            Customer Name:<input type="text" name="name" placeholder="Enter Customer Name" autocomplete="off" required/><br/>
            Address:<input type="text" name="address" placeholder="Enter Address" autocomplete="off" required/><br/>
            Email:<input type="email" name="email_address" placeholder="Enter Email" autocomplete="off" required/><br/>
            Contact Number:<input type="text" name="contact_number" placeholder="Enter Contact Number" autocomplete="off" required/><br/>
            Country of Residences:<input type="text" name="residences" placeholder="Enter Country of Residences" autocomplete="off" required/><br/>
            <input type="submit" name="Submit"/>
        </form>
    </body>
</html>
