<%-- 
    Document   : adminhome
    Created on : 08 14, 18, 12:07:20 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Goldland Millenia Booking and Inventory System</title>
    </head>
    <body>
        <h1>Welcome Back, Admin</h1>
        <h2>Choose an action</h2>
        <a href="admin/createAccount.jsp">Create A New Account</a>
        <a href="admin/ViewAccounts">Manage Accounts</a>
        <a href="admin/addCustomer.jsp">Add Customer Information</a>
        <a href="admin/addEmployee.jsp">Add Employee</a>
    </body>
</html>
