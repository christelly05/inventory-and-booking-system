-- MySQL dump 10.13  Distrib 5.5.47, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: millenia
-- ------------------------------------------------------
-- Server version	5.5.47-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `employee_number` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `account_type` varchar(30) NOT NULL,
  PRIMARY KEY (`employee_number`),
  CONSTRAINT `accounts_ibfk_1` FOREIGN KEY (`employee_number`) REFERENCES `employee` (`employee_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill`
--

DROP TABLE IF EXISTS `bill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill` (
  `bill_id` int(11) NOT NULL AUTO_INCREMENT,
  `serial_number` int(11) NOT NULL,
  `room_service_id` int(11) NOT NULL,
  `room_booked_id` int(11) NOT NULL,
  `other_charges` int(11) DEFAULT NULL,
  `total_price` int(11) NOT NULL,
  PRIMARY KEY (`bill_id`),
  KEY `serial_number` (`serial_number`),
  KEY `room_service_id` (`room_service_id`),
  KEY `room_booked_id` (`room_booked_id`),
  CONSTRAINT `bill_ibfk_1` FOREIGN KEY (`serial_number`) REFERENCES `payment` (`serial_number`),
  CONSTRAINT `bill_ibfk_2` FOREIGN KEY (`room_service_id`) REFERENCES `room_service` (`room_service_id`),
  CONSTRAINT `bill_ibfk_3` FOREIGN KEY (`room_booked_id`) REFERENCES `roombooked` (`room_booked_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill`
--

LOCK TABLES `bill` WRITE;
/*!40000 ALTER TABLE `bill` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking`
--

DROP TABLE IF EXISTS `booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_number` int(11) NOT NULL,
  `room_type` varchar(50) NOT NULL,
  `number_of_rooms_per_type` int(11) NOT NULL,
  `booking_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `checkin_date` date NOT NULL,
  `checkout_date` date NOT NULL,
  `checkin_time` time NOT NULL,
  `checkout_time` time NOT NULL,
  `number_of_guests` int(11) NOT NULL,
  `third_party_agent_name` varchar(100) NOT NULL,
  `special_request` longtext,
  PRIMARY KEY (`transaction_id`),
  KEY `customer_number` (`customer_number`),
  KEY `third_party_agent_name` (`third_party_agent_name`),
  CONSTRAINT `booking_ibfk_1` FOREIGN KEY (`customer_number`) REFERENCES `customer` (`customer_number`),
  CONSTRAINT `booking_ibfk_2` FOREIGN KEY (`third_party_agent_name`) REFERENCES `booking_channel` (`third_party_agent_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking`
--

LOCK TABLES `booking` WRITE;
/*!40000 ALTER TABLE `booking` DISABLE KEYS */;
/*!40000 ALTER TABLE `booking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking_channel`
--

DROP TABLE IF EXISTS `booking_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_channel` (
  `third_party_agent_name` varchar(100) NOT NULL,
  `discount_margin` int(11) NOT NULL,
  `promo` longtext NOT NULL,
  `contact_number` int(11) NOT NULL,
  `email_address` varchar(20) NOT NULL,
  PRIMARY KEY (`third_party_agent_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking_channel`
--

LOCK TABLES `booking_channel` WRITE;
/*!40000 ALTER TABLE `booking_channel` DISABLE KEYS */;
/*!40000 ALTER TABLE `booking_channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `customer_number` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email_address` varchar(20) NOT NULL,
  `contact_number` int(11) NOT NULL,
  `country_of_residences` longtext NOT NULL,
  PRIMARY KEY (`customer_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department` (
  `department_number` int(11) NOT NULL AUTO_INCREMENT,
  `department_name` varchar(50) NOT NULL,
  `department_details` longtext NOT NULL,
  PRIMARY KEY (`department_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `employee_number` int(11) NOT NULL AUTO_INCREMENT,
  `employee_name` varchar(100) NOT NULL,
  `department_number` int(11) NOT NULL,
  `contact_number` int(11) NOT NULL,
  `email` varchar(20) NOT NULL,
  PRIMARY KEY (`employee_number`),
  KEY `department_number` (`department_number`),
  CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`department_number`) REFERENCES `department` (`department_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory`
--

DROP TABLE IF EXISTS `inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory` (
  `stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `restock_id` int(11) NOT NULL,
  `product_number` int(11) NOT NULL,
  `stock_name` varchar(50) NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `inventory_quantity` int(11) NOT NULL,
  PRIMARY KEY (`stock_id`),
  KEY `restock_id` (`restock_id`),
  KEY `product_number` (`product_number`),
  CONSTRAINT `inventory_ibfk_1` FOREIGN KEY (`restock_id`) REFERENCES `restock_products` (`restock_id`),
  CONSTRAINT `inventory_ibfk_2` FOREIGN KEY (`product_number`) REFERENCES `list_of_products` (`product_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory`
--

LOCK TABLES `inventory` WRITE;
/*!40000 ALTER TABLE `inventory` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_details`
--

DROP TABLE IF EXISTS `invoice_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_details` (
  `invoice_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `restock_id` int(11) NOT NULL,
  `supplier_number` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `date_delivered` date NOT NULL,
  `date_ordered` date NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`invoice_details_id`),
  KEY `restock_id` (`restock_id`),
  CONSTRAINT `invoice_details_ibfk_1` FOREIGN KEY (`restock_id`) REFERENCES `restock_products` (`restock_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_details`
--

LOCK TABLES `invoice_details` WRITE;
/*!40000 ALTER TABLE `invoice_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `list_of_products`
--

DROP TABLE IF EXISTS `list_of_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_of_products` (
  `product_number` int(11) NOT NULL AUTO_INCREMENT,
  `product_type` varchar(20) NOT NULL,
  `product_description` longtext NOT NULL,
  `price` int(11) NOT NULL,
  `product_cost` int(11) NOT NULL,
  `category` varchar(30) NOT NULL,
  PRIMARY KEY (`product_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `list_of_products`
--

LOCK TABLES `list_of_products` WRITE;
/*!40000 ALTER TABLE `list_of_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `list_of_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `list_of_service`
--

DROP TABLE IF EXISTS `list_of_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_of_service` (
  `service_number` int(11) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(40) NOT NULL,
  `service_type` varchar(50) NOT NULL,
  `service_details` longtext NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`service_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `list_of_service`
--

LOCK TABLES `list_of_service` WRITE;
/*!40000 ALTER TABLE `list_of_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `list_of_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment` (
  `serial_number` int(11) NOT NULL AUTO_INCREMENT,
  `bank_name` varchar(40) NOT NULL,
  `reference_number` int(11) NOT NULL,
  PRIMARY KEY (`serial_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment`
--

LOCK TABLES `payment` WRITE;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products_used_for_service`
--

DROP TABLE IF EXISTS `products_used_for_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products_used_for_service` (
  `product_number` int(11) NOT NULL,
  `service_item_seq` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` int(11) NOT NULL,
  `unit_measure` int(11) NOT NULL,
  PRIMARY KEY (`product_number`),
  KEY `service_item_seq` (`service_item_seq`),
  CONSTRAINT `products_used_for_service_ibfk_1` FOREIGN KEY (`product_number`) REFERENCES `list_of_products` (`product_number`),
  CONSTRAINT `products_used_for_service_ibfk_2` FOREIGN KEY (`service_item_seq`) REFERENCES `request_details` (`service_item_seq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products_used_for_service`
--

LOCK TABLES `products_used_for_service` WRITE;
/*!40000 ALTER TABLE `products_used_for_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_used_for_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `request_details`
--

DROP TABLE IF EXISTS `request_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `request_details` (
  `service_item_seq` int(11) NOT NULL AUTO_INCREMENT,
  `employee_number` int(11) NOT NULL,
  `room_service_id` int(11) NOT NULL,
  `service_number` int(11) NOT NULL,
  `room_booked_id` int(11) NOT NULL,
  PRIMARY KEY (`service_item_seq`),
  KEY `employee_number` (`employee_number`),
  KEY `room_service_id` (`room_service_id`),
  KEY `service_number` (`service_number`),
  KEY `room_booked_id` (`room_booked_id`),
  CONSTRAINT `request_details_ibfk_1` FOREIGN KEY (`employee_number`) REFERENCES `employee` (`employee_number`),
  CONSTRAINT `request_details_ibfk_2` FOREIGN KEY (`room_service_id`) REFERENCES `room_service` (`room_service_id`),
  CONSTRAINT `request_details_ibfk_3` FOREIGN KEY (`service_number`) REFERENCES `list_of_service` (`service_number`),
  CONSTRAINT `request_details_ibfk_4` FOREIGN KEY (`room_booked_id`) REFERENCES `roombooked` (`room_booked_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `request_details`
--

LOCK TABLES `request_details` WRITE;
/*!40000 ALTER TABLE `request_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `request_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restock_products`
--

DROP TABLE IF EXISTS `restock_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restock_products` (
  `restock_id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_id` int(11) NOT NULL,
  `product_number` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`restock_id`),
  KEY `product_number` (`product_number`),
  KEY `stock_id` (`stock_id`),
  CONSTRAINT `restock_products_ibfk_1` FOREIGN KEY (`product_number`) REFERENCES `list_of_products` (`product_number`),
  CONSTRAINT `restock_products_ibfk_2` FOREIGN KEY (`stock_id`) REFERENCES `inventory` (`stock_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restock_products`
--

LOCK TABLES `restock_products` WRITE;
/*!40000 ALTER TABLE `restock_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `restock_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room`
--

DROP TABLE IF EXISTS `room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room` (
  `room_number` varchar(40) NOT NULL,
  `room_type` varchar(30) NOT NULL,
  `size` int(11) NOT NULL,
  PRIMARY KEY (`room_number`),
  KEY `room_type` (`room_type`),
  CONSTRAINT `room_ibfk_1` FOREIGN KEY (`room_type`) REFERENCES `rtype` (`room_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room`
--

LOCK TABLES `room` WRITE;
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
/*!40000 ALTER TABLE `room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_occupant`
--

DROP TABLE IF EXISTS `room_occupant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room_occupant` (
  `occupant_number` int(11) NOT NULL AUTO_INCREMENT,
  `occupant_name` varchar(100) NOT NULL,
  `contact_number` int(11) NOT NULL,
  PRIMARY KEY (`occupant_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_occupant`
--

LOCK TABLES `room_occupant` WRITE;
/*!40000 ALTER TABLE `room_occupant` DISABLE KEYS */;
/*!40000 ALTER TABLE `room_occupant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_service`
--

DROP TABLE IF EXISTS `room_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room_service` (
  `room_service_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_booked_id` int(11) NOT NULL,
  `date_acquired` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `total_amount` int(11) NOT NULL,
  `number_of_services` int(11) NOT NULL,
  PRIMARY KEY (`room_service_id`),
  KEY `room_booked_id` (`room_booked_id`),
  CONSTRAINT `room_service_ibfk_1` FOREIGN KEY (`room_booked_id`) REFERENCES `roombooked` (`room_booked_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_service`
--

LOCK TABLES `room_service` WRITE;
/*!40000 ALTER TABLE `room_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `room_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roombooked`
--

DROP TABLE IF EXISTS `roombooked`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roombooked` (
  `room_booked_id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) NOT NULL,
  `customer_number` int(11) NOT NULL,
  `occupant_number` int(11) NOT NULL,
  `room_number` varchar(30) NOT NULL,
  `checkout_date` date NOT NULL,
  `checkin_date` date NOT NULL,
  PRIMARY KEY (`room_booked_id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `customer_number` (`customer_number`),
  KEY `occupant_number` (`occupant_number`),
  KEY `room_number` (`room_number`),
  CONSTRAINT `roombooked_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `booking` (`transaction_id`),
  CONSTRAINT `roombooked_ibfk_2` FOREIGN KEY (`customer_number`) REFERENCES `customer` (`customer_number`),
  CONSTRAINT `roombooked_ibfk_3` FOREIGN KEY (`occupant_number`) REFERENCES `room_occupant` (`occupant_number`),
  CONSTRAINT `roombooked_ibfk_4` FOREIGN KEY (`room_number`) REFERENCES `room` (`room_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roombooked`
--

LOCK TABLES `roombooked` WRITE;
/*!40000 ALTER TABLE `roombooked` DISABLE KEYS */;
/*!40000 ALTER TABLE `roombooked` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rtype`
--

DROP TABLE IF EXISTS `rtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rtype` (
  `room_type` varchar(100) NOT NULL,
  `total_number_of_rooms` int(11) NOT NULL,
  `room_description` longtext NOT NULL,
  `room_price` int(11) NOT NULL,
  PRIMARY KEY (`room_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rtype`
--

LOCK TABLES `rtype` WRITE;
/*!40000 ALTER TABLE `rtype` DISABLE KEYS */;
/*!40000 ALTER TABLE `rtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stock_adjustment`
--

DROP TABLE IF EXISTS `stock_adjustment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stock_adjustment` (
  `adjustment_id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_id` int(11) NOT NULL,
  `employee_number` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `date_of_adjustment` date NOT NULL,
  `reason_of_adjustment` longtext NOT NULL,
  PRIMARY KEY (`adjustment_id`),
  KEY `stock_id` (`stock_id`),
  KEY `employee_number` (`employee_number`),
  CONSTRAINT `stock_adjustment_ibfk_1` FOREIGN KEY (`stock_id`) REFERENCES `inventory` (`stock_id`),
  CONSTRAINT `stock_adjustment_ibfk_2` FOREIGN KEY (`employee_number`) REFERENCES `employee` (`employee_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stock_adjustment`
--

LOCK TABLES `stock_adjustment` WRITE;
/*!40000 ALTER TABLE `stock_adjustment` DISABLE KEYS */;
/*!40000 ALTER TABLE `stock_adjustment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier` (
  `supplier_number` int(11) NOT NULL AUTO_INCREMENT,
  `contact_number` int(11) NOT NULL,
  `supplier_name` varchar(50) NOT NULL,
  `products_to_be_supplied` varchar(50) NOT NULL,
  `location` varchar(100) NOT NULL,
  PRIMARY KEY (`supplier_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier`
--

LOCK TABLES `supplier` WRITE;
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-13 11:48:36