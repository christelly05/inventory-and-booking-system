/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.Admin;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pack.Bean.EmployeeBean;
import pack.IO.EmployeeIO;

/**
 *
 * @author user
 */
public class AddEmployee extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String employee_name = request.getParameter("e_name");
            String dept_num = request.getParameter("dept_number");
            String email_add = request.getParameter("e_email_address");
            String contact_num = request.getParameter("contact_number");
            
            String url = "/WEB-INF/view/success.jsp";
            int department_number = 0;
            int contact_number = 0;
            
            try{
                department_number = Integer.parseInt(dept_num);
                contact_number = Integer.parseInt(contact_num);
                
            }catch(Exception e){
                url = "/WEB-INF/view/error.jsp";
            }
            
            EmployeeBean employee = new EmployeeBean();
            EmployeeIO eio = new EmployeeIO();
            
            employee.setEmployee_name(employee_name);
            employee.setDepartment_number(department_number);
            employee.setContact_number(contact_number);
            employee.setEmail(email_add);
            try
            {
                eio.addEmployee(employee);
            }
            catch(Exception e)
            {
                url = "/WEB-INF/view/error.jsp";
            }
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request,response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
