/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.Admin;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pack.Bean.CustomerBean;
import pack.IO.CustomerIO;

/**
 *
 * @author user
 */
public class AddCustomer extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String customer_name = request.getParameter("name");
            String address = request.getParameter("address");
            String email_address = request.getParameter("email_address");
            String contact = request.getParameter("contact_number");
            String country_of_residences = request.getParameter("residences");
            
            String url = "/WEB-INF/view/success.jsp";
            int contact_number = 0;
            
            try{
                contact_number = Integer.parseInt(contact);
            }catch(Exception e){
                e.printStackTrace();
                url = "/WEB-INF/view/error.jsp";
            }
            
            CustomerBean customer = new CustomerBean();
            CustomerIO cio = new CustomerIO();
            
            customer.setCustomer_name(customer_name);
            customer.setAddress(address);
            customer.setEmail_address(email_address);
            customer.setContact_number(contact_number);
            customer.setCountry_of_residences(country_of_residences);
            
            try
            {
                cio.addCustomer(customer);
            }
            catch(Exception e)
            {
                e.printStackTrace();
                url = "/WEB-INF/view/error.jsp";
            }
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request,response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
