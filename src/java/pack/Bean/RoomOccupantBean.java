/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.Bean;

/**
 *
 * @author user
 */
public class RoomOccupantBean {
    private int occupant_number;
    private String occuant_name;
    private int contact_number;
    
    public RoomOccupantBean(){
    }

    public int getOccupant_number() {
        return occupant_number;
    }

    public void setOccupant_number(int occupant_number) {
        this.occupant_number = occupant_number;
    }

    public String getOccuant_name() {
        return occuant_name;
    }

    public void setOccuant_name(String occuant_name) {
        this.occuant_name = occuant_name;
    }

    public int getContact_number() {
        return contact_number;
    }

    public void setContact_number(int contact_number) {
        this.contact_number = contact_number;
    }
}
