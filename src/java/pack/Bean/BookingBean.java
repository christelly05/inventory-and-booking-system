/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.Bean;

import java.sql.Date;
import java.sql.Time;

/**
 *
 * @author user
 */
public class BookingBean {
    private int transaction_id;
    private int customer_number;
    private String room_type;
    private String number_of_rooms_per_type;
    private Date booking_date;
    private Date check_in_date;
    private Date check_out_date;
    private Time check_in_time;
    private Time check_out_time;
    private int number_of_guests;
    private String third_party_agent_name;
    private String special_requests;
    
    public BookingBean(){
    }

    public int getCustomer_number() {
        return customer_number;
    }

    public void setCustomer_number(int customer_number) {
        this.customer_number = customer_number;
    }

    public int getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(int transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getRoom_type() {
        return room_type;
    }

    public void setRoom_type(String room_type) {
        this.room_type = room_type;
    }

    public String getNumber_of_rooms_per_type() {
        return number_of_rooms_per_type;
    }

    public void setNumber_of_rooms_per_type(String number_of_rooms_per_type) {
        this.number_of_rooms_per_type = number_of_rooms_per_type;
    }

    public Date getBooking_date() {
        return booking_date;
    }

    public void setBooking_date(Date booking_date) {
        this.booking_date = booking_date;
    }

    public Date getCheck_in_date() {
        return check_in_date;
    }

    public void setCheck_in_date(Date check_in_date) {
        this.check_in_date = check_in_date;
    }

    public Date getCheck_out_date() {
        return check_out_date;
    }

    public void setCheck_out_date(Date check_out_date) {
        this.check_out_date = check_out_date;
    }

    public Time getCheck_in_time() {
        return check_in_time;
    }

    public void setCheck_in_time(Time check_in_time) {
        this.check_in_time = check_in_time;
    }

    public Time getCheck_out_time() {
        return check_out_time;
    }

    public void setCheck_out_time(Time check_out_time) {
        this.check_out_time = check_out_time;
    }

    public int getNumber_of_guests() {
        return number_of_guests;
    }

    public void setNumber_of_guests(int number_of_guests) {
        this.number_of_guests = number_of_guests;
    }

    public String getThird_party_agent_name() {
        return third_party_agent_name;
    }

    public void setThird_party_agent_name(String third_party_agent_name) {
        this.third_party_agent_name = third_party_agent_name;
    }

    public String getSpecial_requests() {
        return special_requests;
    }

    public void setSpecial_requests(String special_requests) {
        this.special_requests = special_requests;
    }
}
