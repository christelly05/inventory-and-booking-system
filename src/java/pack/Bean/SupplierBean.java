/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.Bean;

/**
 *
 * @author user
 */
public class SupplierBean {
    private int supplier_number;
    private int contact_number;
    private String supplier_name;
    private String products_to_be_supplied;
    private String location;
    
    public SupplierBean(){
    }

    public int getSupplier_number() {
        return supplier_number;
    }

    public void setSupplier_number(int supplier_number) {
        this.supplier_number = supplier_number;
    }

    public int getContact_number() {
        return contact_number;
    }

    public void setContact_number(int contact_number) {
        this.contact_number = contact_number;
    }

    public String getSupplier_name() {
        return supplier_name;
    }

    public void setSupplier_name(String supplier_name) {
        this.supplier_name = supplier_name;
    }

    public String getProducts_to_be_supplied() {
        return products_to_be_supplied;
    }

    public void setProducts_to_be_supplied(String products_to_be_supplied) {
        this.products_to_be_supplied = products_to_be_supplied;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
