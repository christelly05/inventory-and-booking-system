/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.Bean;

/**
 *
 * @author user
 */
public class ProductsUsedForServiceBean {
    private int product_number;
    private int service_item_seq;
    private int quantity;
    private int unit_price;
    private int unit_measure;
    
    public ProductsUsedForServiceBean(){
    }

    public int getProduct_number() {
        return product_number;
    }

    public void setProduct_number(int product_number) {
        this.product_number = product_number;
    }

    public int getService_item_seq() {
        return service_item_seq;
    }

    public void setService_item_seq(int service_item_seq) {
        this.service_item_seq = service_item_seq;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(int unit_price) {
        this.unit_price = unit_price;
    }

    public int getUnit_measure() {
        return unit_measure;
    }

    public void setUnit_measure(int unit_measure) {
        this.unit_measure = unit_measure;
    }
}
