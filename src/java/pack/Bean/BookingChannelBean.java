/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.Bean;

/**
 *
 * @author user
 */
public class BookingChannelBean {
    private String third_party_agent_name;
    private int discount_marigin;
    private String promo_package;
    private int contact_number;
    private String email_address;
    
    public BookingChannelBean(){
    }

    public String getThird_party_agent_name() {
        return third_party_agent_name;
    }

    public void setThird_party_agent_name(String third_party_agent_name) {
        this.third_party_agent_name = third_party_agent_name;
    }

    public int getDiscount_marigin() {
        return discount_marigin;
    }

    public void setDiscount_marigin(int discount_marigin) {
        this.discount_marigin = discount_marigin;
    }

    public String getPromo_package() {
        return promo_package;
    }

    public void setPromo_package(String promo_package) {
        this.promo_package = promo_package;
    }

    public int getContact_number() {
        return contact_number;
    }

    public void setContact_number(int contact_number) {
        this.contact_number = contact_number;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }        
}
