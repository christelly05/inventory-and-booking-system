/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.Bean;

/**
 *
 * @author user
 */
public class CustomerBean {
    private int customer_number;
    private String customer_name;
    private String address;
    private String email_address;
    private int contact_number;
    private String country_of_residences;
    
    public CustomerBean(){
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }
    
    public int getCustomer_number() {
        return customer_number;
    }

    public void setCustomer_number(int custmer_number) {
        this.customer_number = custmer_number; 
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getContact_number() {
        return contact_number;
    }

    public void setContact_number(int contact_number) {
        this.contact_number = contact_number;
    }

    public String getCountry_of_residences() {
        return country_of_residences;
    }

    public void setCountry_of_residences(String country_of_residences) {
        this.country_of_residences = country_of_residences;
    } 
}
