/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.Bean;

import java.sql.Date;

/**
 *
 * @author user
 */
public class RoomServiceBean {
    private int room_service_id;
    private int room_booked_id;
    private Date date_acquired;
    private int total_amount;
    private int number_of_servces;
    
    public RoomServiceBean(){
    }

    public int getRoom_service_id() {
        return room_service_id;
    }

    public void setRoom_service_id(int room_service_id) {
        this.room_service_id = room_service_id;
    }

    public int getRoom_booked_id() {
        return room_booked_id;
    }

    public void setRoom_booked_id(int room_booked_id) {
        this.room_booked_id = room_booked_id;
    }

    public Date getDate_acquired() {
        return date_acquired;
    }

    public void setDate_acquired(Date date_acquired) {
        this.date_acquired = date_acquired;
    }

    public int getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(int total_amount) {
        this.total_amount = total_amount;
    }

    public int getNumber_of_servces() {
        return number_of_servces;
    }

    public void setNumber_of_servces(int number_of_servces) {
        this.number_of_servces = number_of_servces;
    }
}
