/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.Bean;

/**
 *
 * @author user
 */
public class PaymentBean {
    private int serial_number;
    private String bank_name;
    private int deposit_number;
    
    public PaymentBean(){
    }

    public int getSerial_number() {
        return serial_number;
    }

    public void setSerial_number(int serial_number) {
        this.serial_number = serial_number;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public int getDeposit_number() {
        return deposit_number;
    }

    public void setDeposit_number(int deposit_number) {
        this.deposit_number = deposit_number;
    }
}
