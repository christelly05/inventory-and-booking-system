/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.Bean;

/**
 *
 * @author user
 */
public class RoomTypeBean {
    private int room_type;
    private int total_number_of_rooms;
    private String room_description;
    private int room_price;
    
    public RoomTypeBean(){
    }

    public int getRoom_type() {
        return room_type;
    }

    public void setRoom_type(int room_type) {
        this.room_type = room_type;
    }

    public int getTotal_number_of_rooms() {
        return total_number_of_rooms;
    }

    public void setTotal_number_of_rooms(int total_number_of_rooms) {
        this.total_number_of_rooms = total_number_of_rooms;
    }

    public String getRoom_description() {
        return room_description;
    }

    public void setRoom_description(String room_description) {
        this.room_description = room_description;
    }

    public int getRoom_price() {
        return room_price;
    }

    public void setRoom_price(int room_price) {
        this.room_price = room_price;
    }
}
