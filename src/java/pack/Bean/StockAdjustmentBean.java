/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.Bean;

import java.sql.Date;

/**
 *
 * @author user
 */
public class StockAdjustmentBean {
    private int adjustment_id;
    private int stock_id;
    private int employee_number;
    private int quantity;
    private Date date_of_adjustment;
    private String reason_of_adjustment;
    
    public StockAdjustmentBean(){
    }

    public int getAdjustment_id() {
        return adjustment_id;
    }

    public void setAdjustment_id(int adjustment_id) {
        this.adjustment_id = adjustment_id;
    }

    public int getStock_id() {
        return stock_id;
    }

    public void setStock_id(int stock_id) {
        this.stock_id = stock_id;
    }

    public int getEmployee_number() {
        return employee_number;
    }

    public void setEmployee_number(int employee_number) {
        this.employee_number = employee_number;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getDate_of_adjustment() {
        return date_of_adjustment;
    }

    public void setDate_of_adjustment(Date date_of_adjustment) {
        this.date_of_adjustment = date_of_adjustment;
    }

    public String getReason_of_adjustment() {
        return reason_of_adjustment;
    }

    public void setReason_of_adjustment(String reason_of_adjustment) {
        this.reason_of_adjustment = reason_of_adjustment;
    }
}
