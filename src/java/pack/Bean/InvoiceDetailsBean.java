/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.Bean;

import java.sql.Date;

/**
 *
 * @author user
 */
public class InvoiceDetailsBean {
    private int invoice_details_id;
    private int stock_id;
    private int supplier_number;
    private int quantity;
    private Date date_delivered;
    private Date date_ordered;
    private int price;
    
    public InvoiceDetailsBean(){
    }

    public int getInvoice_details_id() {
        return invoice_details_id;
    }

    public void setInvoice_details_id(int invoice_details_id) {
        this.invoice_details_id = invoice_details_id;
    }

    public int getStock_id() {
        return stock_id;
    }

    public void setStock_id(int stock_id) {
        this.stock_id = stock_id;
    }

    public int getSupplier_number() {
        return supplier_number;
    }

    public void setSupplier_number(int supplier_number) {
        this.supplier_number = supplier_number;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getDate_delivered() {
        return date_delivered;
    }

    public void setDate_delivered(Date date_delivered) {
        this.date_delivered = date_delivered;
    }

    public Date getDate_ordered() {
        return date_ordered;
    }

    public void setDate_ordered(Date date_ordered) {
        this.date_ordered = date_ordered;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
