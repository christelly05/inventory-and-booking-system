/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.Bean;

import java.sql.Date;

/**
 *
 * @author user
 */
public class RoomBookedBean {
    private int room_booked_id;
    private int transaction_id;
    private int occupant_nummber;
    private int room_number;
    private Date check_in_date;
    private Date check_out_date;
    
    public RoomBookedBean(){
    }

    public int getRoom_booked_id() {
        return room_booked_id;
    }

    public void setRoom_booked_id(int room_booked_id) {
        this.room_booked_id = room_booked_id;
    }

    public int getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(int transaction_id) {
        this.transaction_id = transaction_id;
    }

    public int getOccupant_nummber() {
        return occupant_nummber;
    }

    public void setOccupant_nummber(int occupant_nummber) {
        this.occupant_nummber = occupant_nummber;
    }

    public int getRoom_number() {
        return room_number;
    }

    public void setRoom_number(int room_number) {
        this.room_number = room_number;
    }

    public Date getCheck_in_date() {
        return check_in_date;
    }

    public void setCheck_in_date(Date check_in_date) {
        this.check_in_date = check_in_date;
    }

    public Date getCheck_out_date() {
        return check_out_date;
    }

    public void setCheck_out_date(Date check_out_date) {
        this.check_out_date = check_out_date;
    }
}
    
