/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.Bean;

/**
 *
 * @author user
 */
public class AccountsBean {
    private int employee_number;
    private String username;
    private String password;
    private String account_type;
    private int IsEnabled;
    
    public AccountsBean(){
    }

    public int getIsEnabled() {
        return IsEnabled;
    }

    public void setIsEnabled(int IsEnabled) {
        this.IsEnabled = IsEnabled;
    }
    

    public String getAccount_type() {
        return account_type;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }
    
    public int getEmployee_number() {
        return employee_number;
    }

    public void setEmployee_number(int employe_number) {
        this.employee_number = employe_number;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
