/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.Bean;

/**
 *
 * @author user
 */
public class RequestDetailsBean {
    private int service_item_seq;
    private int employee_number;
    private int room_service_id;
    private int service_number;
    private int room_booked_id;
    
    public RequestDetailsBean(){
    }

    public int getService_item_seq() {
        return service_item_seq;
    }

    public void setService_item_seq(int service_item_seq) {
        this.service_item_seq = service_item_seq;
    }

    public int getEmployee_number() {
        return employee_number;
    }

    public void setEmployee_number(int employee_number) {
        this.employee_number = employee_number;
    }

    public int getRoom_service_id() {
        return room_service_id;
    }

    public void setRoom_service_id(int room_service_id) {
        this.room_service_id = room_service_id;
    }

    public int getService_number() {
        return service_number;
    }

    public void setService_number(int service_number) {
        this.service_number = service_number;
    }

    public int getRoom_booked_id() {
        return room_booked_id;
    }

    public void setRoom_booked_id(int room_booked_id) {
        this.room_booked_id = room_booked_id;
    }
}
