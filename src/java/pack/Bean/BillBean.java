/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.Bean;

/**
 *
 * @author user
 */
public class BillBean {
    private int bill_id;
    private int serial_number;
    private int room_service_number;
    private int room_booked_id;
    private int other_charges;
    private int total_price;
    
    public BillBean(){
    }

    public int getBill_id() {
        return bill_id;
    }

    public void setBill_id(int bill_id) {
        this.bill_id = bill_id;
    }

    public int getSerial_number() {
        return serial_number;
    }

    public void setSerial_number(int serial_number) {
        this.serial_number = serial_number;
    }

    public int getRoom_service_number() {
        return room_service_number;
    }

    public void setRoom_service_number(int room_service_number) {
        this.room_service_number = room_service_number;
    }

    public int getRoom_booked_id() {
        return room_booked_id;
    }

    public void setRoom_booked_id(int room_booked_id) {
        this.room_booked_id = room_booked_id;
    }

    public int getOther_charges() {
        return other_charges;
    }

    public void setOther_charges(int other_charges) {
        this.other_charges = other_charges;
    }

    public int getTotal_price() {
        return total_price;
    }

    public void setTotal_price(int total_price) {
        this.total_price = total_price;
    }
}
