/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.Bean;

/**
 *
 * @author user
 */
public class DepartmentBean {
    private int department_number;
    private String deparment_name;
    private String department_details;
    
    public DepartmentBean(){
    }

    public int getDepartment_number() {
        return department_number;
    }

    public void setDepartment_number(int department_number) {
        this.department_number = department_number;
    }

    public String getDeparment_name() {
        return deparment_name;
    }

    public void setDeparment_name(String deparment_name) {
        this.deparment_name = deparment_name;
    }

    public String getDepartment_details() {
        return department_details;
    }

    public void setDepartment_details(String department_details) {
        this.department_details = department_details;
    }
}
