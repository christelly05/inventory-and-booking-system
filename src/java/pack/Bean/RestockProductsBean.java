/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.Bean;

/**
 *
 * @author user
 */
public class RestockProductsBean {
    private int restock_id;
    private int stock_id;
    private int product_number;
    private int quantity;
    
    public RestockProductsBean(){
    }

    public int getRestock_id() {
        return restock_id;
    }

    public void setRestock_id(int restock_id) {
        this.restock_id = restock_id;
    }

    public int getStock_id() {
        return stock_id;
    }

    public void setStock_id(int stock_id) {
        this.stock_id = stock_id;
    }

    public int getProduct_number() {
        return product_number;
    }

    public void setProduct_number(int product_number) {
        this.product_number = product_number;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
