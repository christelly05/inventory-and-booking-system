/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.IO;

import java.sql.PreparedStatement;
import pack.Bean.BookingBean;

/**
 *
 * @author user
 */
public class BookingFormIO {
    DatabaseLayer db;
    
    public BookingFormIO(){
        db = new DatabaseLayer();
    }
    
    public int addBooking(BookingBean booking){
        int count = 0;
        try {
            db.connection();
            String preparedSQL = "insert into Booking(transaction_id, customer_number, room_type, number_of_rooms_per_type, booking_date, checkin_date, checkout_date, checkin_time, checkout_time, number_of_guests, third_party_agent_name, special_request) values(?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps = db.getUpdateStatement(preparedSQL);

            ps.setInt(1, booking.getTransaction_id());
            ps.setInt(2, booking.getCustomer_number());
            ps.setString(3, booking.getRoom_type());
            count = ps.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.closeConnection();
        }
        return count;
    }
}
