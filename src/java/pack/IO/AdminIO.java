/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.IO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import pack.Bean.*;
/**
 *
 * @author user
 */
public class AdminIO {
    DatabaseLayer db;
    
    public AdminIO()
    {
        db = new DatabaseLayer();
    }
    
    private boolean same(AccountsBean sbparam)
    {
        return getAccountByEmployeeNumber(sbparam.getEmployee_number()) != null;
    }
    
    public int addAccount(AccountsBean param)
    {
        if(same(param))
        {
            return 0;
        }
        
        try
        {
            db.connection();
            String sql = "insert into accounts(employee_number, username, password, account_type, isEnabled) values (?,?,?,?,?)";
            PreparedStatement ps = db.getUpdateStatement(sql);
                        
            ps.setInt(1, param.getEmployee_number());
            ps.setString(2,param.getUsername());
            ps.setString(3, param.getPassword());
            ps.setString(4, param.getAccount_type());
            ps.setInt(5, param.getIsEnabled());
            ps.executeUpdate();
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        finally
        {
            db.closeConnection();
        }
        return 1;
    }
    
    public AccountsBean getAccountByEmployeeNumber(int param)
    {
        AccountsBean ab = null;
        try
        {
            db.connection();
            String sql = "select * from accounts where employee_number = ?";
            PreparedStatement ps = db.getUpdateStatement(sql);
            ps.setInt(1, param);
            ResultSet rs = ps.executeQuery();
            
            if(rs.next())
            {
                int employee_number = rs.getInt("employee_number");
                String username = rs.getString("username");
                String password = rs.getString("password");
                String account_type = rs.getString("account_type");
                
                ab = new AccountsBean();
                ab.setEmployee_number(employee_number);
                ab.setUsername(username);
                ab.setPassword(password);
                ab.setAccount_type(account_type);
            }
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        finally
        {
            db.closeConnection();
        }
        
        return ab;
    }
    
    public int getEmployeeNumberByName(String name)
    {
        int id = 0;
        try
        {
            db.connection();
            String sql = "select employee_number from employee where employee.employee_name = ?";
            PreparedStatement ps = db.getUpdateStatement(sql);
            ps.setString(1,name);
            ResultSet rs = ps.executeQuery();
            
            while(rs.next())
            {
                id = rs.getInt("employee_number");
            }
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        finally
        {
            db.closeConnection();
        }
        return id;
    }
}
