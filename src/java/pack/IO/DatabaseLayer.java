/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.IO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 *
 * @author user
 */
public class DatabaseLayer {
    private String driver;
    private String url;
    private String user;
    private String pass;
    private Connection conn;
    
    public DatabaseLayer () {
        ResourceBundle sql = ResourceBundle.getBundle("pack.IO.SQL");
        this.url = sql.getString("url");
        this.user = sql.getString("user");
        this.pass = sql.getString("password");
        this.driver = sql.getString("driver");
    }
    
    public void connection() {
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url, user, pass); 
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void closeConnection(){
        try {
            if(conn != null){
                conn.close();
            }
        } catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public PreparedStatement getUpdateStatement(String sqlString) {
        if(conn==null) return null;
        try {
            return conn.prepareStatement(sqlString);
        } catch(SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
