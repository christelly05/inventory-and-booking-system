/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.IO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import pack.Bean.AccountsBean;

/**
 *
 * @author user
 */
public class AccountIO {
    DatabaseLayer db;
    
    public AccountIO()
    {
        db = new DatabaseLayer();
    }
    
    private AccountsBean getAccount()
    {
        AccountsBean ab = null;
        try
        {
            db.connection();
            String sql = "select * from accounts";
            PreparedStatement ps = db.getUpdateStatement(sql);
            ResultSet rs = ps.executeQuery();
            
            if(rs.next())
            {
                int employee_number = rs.getInt("employee_number");
                String username = rs.getString("username");
                String password = rs.getString("password");
                String account_type = rs.getString("account_type");
                int isenabled = rs.getInt("isEnabled");
                
                ab = new AccountsBean();
                ab.setEmployee_number(employee_number);
                ab.setUsername(username);
                ab.setPassword(password);
                ab.setAccount_type(account_type);
                ab.setIsEnabled(isenabled);
            }
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        finally
        {
            db.closeConnection();
        }
        
        return ab;
    }
    
    public boolean validAccount(String username, String password)
    {
        AccountsBean abb = getAccount();
        
        return (abb.getUsername().equals(username)) && ((abb.getPassword().equals(password)) && (abb.getIsEnabled() == 1));
    }
    
    public LinkedList<AccountsBean> getAllAccounts()
    {
        LinkedList<AccountsBean> list = new LinkedList<>();
        try 
        {
            db.connection();
            String sql = "select * from accounts";
            PreparedStatement ps = db.getUpdateStatement(sql);
            ResultSet rs = ps.executeQuery();

            while(rs.next())
            {
                int id = rs.getInt("employee_number");
                String username = rs.getString("username");
                String password = rs.getString("password");
                String account_type = rs.getString("account_type");
                int isEnabled = rs.getInt("isEnabled");
                
                AccountsBean sb = new AccountsBean();
                sb.setEmployee_number(id);
                sb.setUsername(username);
                sb.setPassword(password);
                sb.setAccount_type(account_type);
                sb.setIsEnabled(isEnabled);
                
                list.add(sb);
            }
        } 
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        finally {
            db.closeConnection();
        }

        return list;
    }
    
    public LinkedList<AccountsBean> getEnabledAccounts()
    {
        LinkedList<AccountsBean> list = new LinkedList<>();
        try 
        {
            db.connection();
            String sql = "select * from accounts where isEnabled = 1";
            PreparedStatement ps = db.getUpdateStatement(sql);
            ResultSet rs = ps.executeQuery();

            while(rs.next())
            {
                int id = rs.getInt("employee_number");
                String username = rs.getString("username");
                String password = rs.getString("password");
                String account_type = rs.getString("account_type");
                int isEnabled = rs.getInt("isEnabled");
                
                AccountsBean sb = new AccountsBean();
                sb.setEmployee_number(id);
                sb.setUsername(username);
                sb.setPassword(password);
                sb.setAccount_type(account_type);
                sb.setIsEnabled(isEnabled);
                
                list.add(sb);
            }
        } 
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        finally {
            db.closeConnection();
        }

        return list;
    }
    
    public LinkedList<AccountsBean> getDisabledAccounts()
    {
        LinkedList<AccountsBean> list = new LinkedList<>();
        try 
        {
            db.connection();
            String sql = "select * from accounts where isEnabled = 0";
            PreparedStatement ps = db.getUpdateStatement(sql);
            ResultSet rs = ps.executeQuery();

            while(rs.next())
            {
                int id = rs.getInt("employee_number");
                String username = rs.getString("username");
                String password = rs.getString("password");
                String account_type = rs.getString("account_type");
                int isEnabled = rs.getInt("isEnabled");
                
                AccountsBean sb = new AccountsBean();
                sb.setEmployee_number(id);
                sb.setUsername(username);
                sb.setPassword(password);
                sb.setAccount_type(account_type);
                sb.setIsEnabled(isEnabled);
                
                list.add(sb);
            }
        } 
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        finally {
            db.closeConnection();
        }

        return list;
    }
}