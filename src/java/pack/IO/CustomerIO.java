/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.IO;

import java.sql.PreparedStatement;
import pack.Bean.CustomerBean;

/**
 *
 * @author user
 */
public class CustomerIO {
    DatabaseLayer db;
    
    public CustomerIO(){
        db = new DatabaseLayer();
    }
    
    public void addCustomer(CustomerBean customer){
        try {
            db.connection();
            String preparedSQL = "insert into customer(customer_number, customer_name, address, email_address, contact_number, country_of_residences) values(?,?,?,?,?,?)";
            PreparedStatement ps = db.getUpdateStatement(preparedSQL);

            ps.setInt(1, customer.getCustomer_number());
            ps.setString(2, customer.getCustomer_name());
            ps.setString(3, customer.getAddress());
            ps.setString(4, customer.getEmail_address());
            ps.setInt(5, customer.getContact_number());
            ps.setString(6, customer.getCountry_of_residences());
            ps.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.closeConnection();
        }
    }
}
