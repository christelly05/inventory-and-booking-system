/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack.IO;

import java.io.IOException;
import java.sql.PreparedStatement;
import pack.Bean.EmployeeBean;

/**
 *
 * @author user
 */
public class EmployeeIO{

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    DatabaseLayer db;
    
    public EmployeeIO(){
        db = new DatabaseLayer();
    }
    
    public void addEmployee(EmployeeBean employee){
        try {
            db.connection();
            String preparedSQL = "insert into employee(employee_number, employee_name, department_number, contact_number, email) values(?,?,?,?,?)";
            PreparedStatement ps = db.getUpdateStatement(preparedSQL);

            ps.setInt(1, employee.getEmployee_number());
            ps.setString(2, employee.getEmployee_name());
            ps.setInt(3, employee.getDepartment_number());
            ps.setInt(4, employee.getContact_number());
            ps.setString(5, employee.getEmail());
            ps.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.closeConnection();
        }
    }
}
